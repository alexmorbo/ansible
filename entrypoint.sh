#!/bin/bash

# This command wrapper sets up SSH config files based on the following
# environment variables:
#
#   SSH_CONFIG - contents of an SSH config file
#   SSH_KNOWN_HOSTS - contents of a SSH known_hosts file
#   SSH_PRIVATE_RSA_KEY - contents of a SSH private RSA key
#   SSH_PRIVATE_DSA_KEY - contents of a SSH private DSA key
#   SSH_DEBUG - switch to a high debug level 3 for all hosts, to help solve SSH issues
#
# The environment variables are unset after the files are created to help
# prevent accidental output in logs

set -e

mkdir -p ~/.ssh
chmod 700 ~/.ssh

decode_base64() {
  # Determine the platform dependent base64 decode argument
  if [[ "$(echo 'eA==' | base64 -d 2> /dev/null)" = 'x' ]]; then
    local BASE64_DECODE_ARG='-d'
  else
    local BASE64_DECODE_ARG='--decode'
  fi

  echo "$1" | tr -d '\n' | base64 "$BASE64_DECODE_ARG"
}

## ~/.ssh/config
if [[ ! -z "$SSH_CONFIG" ]]; then
    echo "$SSH_CONFIG" > ~/.ssh/config
    chmod 600 ~/.ssh/config
    unset SSH_CONFIG
fi

if [[ ! -z "$SSH_CONFIG_B64" ]]; then
    decode_base64 "$SSH_CONFIG_B64" > ~/.ssh/config
    chmod 600 ~/.ssh/config
    unset SSH_CONFIG_B64
fi

## ~/.ssh/known_hosts
if [[ ! -z "$SSH_KNOWN_HOSTS" ]]; then
    echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts
    chmod 600 ~/.ssh/known_hosts
    unset SSH_KNOWN_HOSTS
fi

if [[ ! -z "$SSH_KNOWN_HOSTS_B64" ]]; then
    decode_base64 "$SSH_KNOWN_HOSTS_B64" > ~/.ssh/known_hosts
    chmod 600 ~/.ssh/known_hosts
    unset SSH_KNOWN_HOSTS_B64
fi

## ~/.ssh/id_rsa
if [[ ! -z "$SSH_PRIVATE_RSA_KEY" ]]; then
    echo "$SSH_PRIVATE_RSA_KEY" > ~/.ssh/id_rsa
    chmod 600 ~/.ssh/id_rsa
    unset SSH_PRIVATE_RSA_KEY
fi

if [[ ! -z "$SSH_PRIVATE_RSA_KEY_B64" ]]; then
    decode_base64 "$SSH_PRIVATE_RSA_KEY_B64" > ~/.ssh/id_rsa
    chmod 600 ~/.ssh/id_rsa
    unset SSH_PRIVATE_RSA_KEY_B64
fi

## ~/.ssh/id_dsa
if [[ ! -z "$SSH_PRIVATE_DSA_KEY" ]]; then
    echo "$SSH_PRIVATE_DSA_KEY" > ~/.ssh/id_dsa
    chmod 600 ~/.ssh/id_dsa
    unset SSH_PRIVATE_DSA_KEY
fi

if [[ ! -z "$SSH_PRIVATE_DSA_KEY_B64" ]]; then
    decode_base64 "$SSH_PRIVATE_DSA_KEY_B64" > ~/.ssh/id_dsa
    chmod 600 ~/.ssh/id_dsa
    unset SSH_PRIVATE_DSA_KEY_B64
fi

## ssh debug mode
if [[ ! -z "$SSH_DEBUG" ]]; then
    touch ~/.ssh/config
    chmod 600 ~/.ssh/config
    echo -e "Host *\n  LogLevel DEBUG3" >> ~/.ssh/config
    unset SSH_DEBUG
fi

## ansible vault
if [[ ! -z "$ANSIBLE_VAULT_PWD_B64" ]]; then
    decode_base64 "$ANSIBLE_VAULT_PWD_B64" > ~/.ansible_vault_password
    chmod 600 ~/.ansible_vault_password
    unset ANSIBLE_VAULT_PWD_B64
fi
if [[ ! -z "$ANSIBLE_VAULT_PWD" ]]; then
    echo "$ANSIBLE_VAULT_PWD" > ~/.ansible_vault_password
    chmod 600 ~/.ansible_vault_password
    unset ANSIBLE_VAULT_PWD
fi

[[ $1 ]] && exec "$@"

