FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND noninteractive

RUN set -xe; \
    apt-get update -q -y && \
    apt-get upgrade -q -y && \
    apt-get -q -y install wget curl ansible git rsync openssh-client python3-distutils python3-distutils-extra sshpass --no-install-recommends && \
    apt-get clean autoclean && \
    rm -rf /var/lib/cache/* && \
    rm -rf /var/lib/apt/lists/*

ADD entrypoint.sh /usr/bin/entrypoint

RUN chmod +x /usr/bin/entrypoint

ENTRYPOINT ["/usr/bin/entrypoint"]
